const PubSub = require("myps.client");
const colors = require("colors");
const date = require("date-and-time");
const EE = require("events");

let LogViewer = params => {
  let self = new EE();

  let _params = {
    socketFile: `${process.cwd()}/myipc.sock`,
    channel: "logs",
    levels: ["info", "debug", "error"],
    sourceFilter: "all"
  };

  Object.assign(_params, params);

  // TODO: socketfile
  let _pubsub = PubSub({
    socketFile: _params.socketFile
  });
  _pubsub.on("error", e => {
    self.emit("error");
  });
  _pubsub.on("close", _ => {
    self.emit("close");
  });
  _pubsub.on("end", _ => {
    self.emit("end");
  });

  _pubsub.on("connect", async connection_id => {
    console.log("connected");
    self.connection_id = connection_id;
    await _pubsub.subscribe(_params.channel);
  });
  _pubsub.on("message", async (topic, message, connection_id) => {
    if (connection_id === self.connection_id) {
      return;
    }
    try {
      let { level, source, timestamp, payload } = JSON.parse(message);
      // filter by level
      if (_params.levels.includes(level)) {
        // filter by sourceName
        if (_params.sourceFilter === source || _params.sourceFilter === "all") {
          let logStr = `${source}:: ${level}:: ${date.format(new Date(timestamp), "HH:mm:ss")}:: ${payload}`;
          switch (level) {
            case "debug":
              logStr = logStr.green;
              break;
            case "info":
              logStr = logStr.blue;
              break;
            case "error":
              logStr = logStr.red;
              break;
            default:
              break;
          }
          console.log(logStr);
        }
      }
    } catch (e) {
      console.log(`Error while processing incoming message: ${e.message}`);
    }
  });

  return self;
};

module.exports = LogViewer;
