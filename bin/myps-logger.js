#!/usr/bin/env node

const program = require("commander");

program
  .option("-c --channel <channel>", "channel name to listen. Default: logs.")
  .option("-s --socketfile <path>", "path to myipc.sock file. Default: ${process.cwd()}/myipc.sock.")
  .parse(process.argv);

let params = {};
params.channel = "logs";
params.socketFile = "/run/myps/myipc.sock";

if (Object.prototype.hasOwnProperty.call(program, "socketfile")) {
  params.socketFile = program["socketfile"];
}
if (Object.prototype.hasOwnProperty.call(program, "channel")) {
  params.channel = program["channel"];
}

require("../index")(params);
